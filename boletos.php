<?php include_once('header.php'); ?>

<?php

$subTitle = 'Cliente Cipa';
$title = '2ª Via de Boleto – Condomínios';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<main class="main_boletos my-5">
  <section class="boleto_content_text">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <p>Perdeu o boleto do seu condomínio ou esqueceu de pagar? Sem problemas, acesse via digital o CIPA Fácil. Você pode
          imprimir seu boleto ou utilizar a linha digitável para pagamento pelo site ou app do seu banco.</p>
          <p>Para acessar o CIPA Fácil é muito simples, basta clicar em “acessar”, na parte superior do site e em seguida você será
          redirecionado para a página de login e senha</p>

        </div>
      </div>
    </div>
  </section>

  <section class="boleto_content_demo mt-8 mb-4">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-10">
          <div class="title_bg_gray d-none d-lg-block"></div>
          <h2 class="mb-6">SEU LOGIN E SENHA ESTÃO DISPONÍVEIS NO BOLETO DE COTA CONDOMINIAL</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <img src="assets/images/boletos/2-via-de-boleto.png" alt="" class="img-fluid">
        </div>
      </div>
    </div>
  </section>


</main>

<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
