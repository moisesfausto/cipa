<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cipa</title>

    <link rel="stylesheet" href="cdn/css/bootstrap_person.css">
    <link rel="stylesheet" href="cdn/css/app.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;600;700;800;900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="assets/fontawesome/css/all.min.css">

</head>
<body>

<header class="main_header bg_front">
    <div class="customer_area">
        <div class="container">
            <div class="row justify-content-end align-items-center">
                <div class="col-md-2 col-lg-3 col-xl-2 my-2 my-md-0 px-md-0 text-center text-md-left" style="line-height: 1;">
                    <label class="mb-0">área do cliente:</label>
                    <span class="">Condomínios</span>
                </div>
                <div class="col-md-5 col-lg-4 col-xl-3 mb-2">
                    <div class="account rounded-pill mx-auto">
                        <button type="button" class="btn btn-tsuru-blue btn-lg rounded-pill"><span>Acessar conta:</span>
                            <br> CIPA Fácil</button>
                        <img src="assets/images/icones/arrow.svg" alt="">
                    </div>
                </div>

                <div class="col-md-5 col-lg-4 col-xl-3">
                    <div class="services rounded-pill mx-auto">
                        <a href="#" class="btn btn-tsuru-red btn-lg rounded-pill" role="button">Outros serviços</a>
                        <a href="#" class="btn btn-tsuru-red btn-lg rounded-pill d-none" role="button"><span>Outros
                                serviços do Grupo</span> CIPA Locação</a>
                        <img src="assets/images/icones/arrow.svg" alt="">

                        <a href="#" class="btn btn-tsuru-red btn-lg rounded-pill d-none" role="button"
                            style="padding: 20px 28px;">CIPA Vendas</a>
                        <a href="#" class="btn btn-tsuru-red btn-lg rounded-pill d-none" role="button">CIPA Corretora de
                            Seguros</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="main_menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12 px-0">
                <nav class="navbar navbar-expand-xl">

                    <div class="col-md-6 col-lg-4 col-xl-4 px-0">
                        <div class="main_menu_logo d-flex justify-content-between">
                            <a class="navbar-brand" href="#"><img src="assets/images/logo/logo-cipa-header.svg"alt="Cipa"></a>
                            <button class="navbar-toggler d-md-none" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>


                    <div class="col-md-6 col-lg-8 col-xl-8 px-0">
                        <div class="main_menu_search">
                            <div class="input-group mb-3 mt-sm-3">
                                <input type="text" class="form-control" placeholder="O que você procura?"
                                    aria-label="O que você procura?" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i
                                            class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="collapse navbar-collapse main_menu_navbar" id="navbarNavAltMarkup">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="/">home</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="#" >
                                        conteúdo exclusivo
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="blog.php">• Blog</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="ebooks.php">• e-book</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="cipa-na-midia.php">• CIPA na mídia</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="videos.php">• vídeos</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="blog.php">blog condomínio</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="nossa-historia.php">a empresa</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#" >cliente cipa</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="boleto.php">• 2.º via de Boleto</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="debito.php">• Débito Automático</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="cadastro.php">• Atualização de Cadastro</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="ouvidoria.php">• Ouvidoria</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="parceiros.php">• Parceiros</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contato.php">contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </nav>

                <nav class="navbar navbar-expand-md d-none d-sm-none d-md-block d-xl-none">


                    <div class="col-md-12  px-0">
                        <div class="collapse navbar-collapse main_menu_navbar justify-content-end" id="navbarNavAltMarkup">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Home</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link " href="#" id="navbarDropdown" role="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Conteúdo gratuito
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="blog.php">• Blog</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="ebook.php">• e-book</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="cipa-na-midia.php">• CIPA na mídia</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="videos.php">• vídeos</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Condomínio etc</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="nossa-historia.php">a empresa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="">cliente cipa</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contato.php">contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </nav>

            </div>

        </div>
    </div>
</div>
