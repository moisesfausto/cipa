<?php include_once('header.php'); ?>

<?php

$subTitle = 'Cliente Cipa';
$title = 'Parceiros';
$description = '';

/**
 * ATENÇÃO
 * As imagens deve ser salvas em tamanho 300x300
 */

?>
<?php include_once('widgets/page-title.php'); ?>

<div class="main_parceiros my-5">
  <div class="container">
    <div class="row">

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/abadi.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">ABADI</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/secovirio.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">SECOVIRIO</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/ral.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">ral</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/ademirj.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">ademirj</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/federacao.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">federação</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-lg-4 my-2 my-lg-5">
        <div class="parceiro_cipa text-center">
          <img src="assets/images/parceiros/page-parceiros/fiabci-brasil.png" alt="ABADI" class="img-fluid mb-3 mb-lg-5">
          <h2 class="mb-3 mb-lg-4">fiabci-brasil</h2>
          <p>A CIPA disponibiliza um canal exclusivo com os clientes. Por meio da ouvidoria, cliente CIPA pode tirar suas dúvidas, fazer elogiou ou reclamações.</p>
        </div>
      </div>

    </div>
  </div>
</div>


<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
