<?php include_once('header.php'); ?>

<?php

$subTitle = 'Cliente Cipa';
$title = 'Adesão Débito Automático';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<section class="main_debito my-8">

<div class="container">

  <div class="row">
    <div class="col-12 col-lg-7">
        <div class="title_bg_gray d-none d-lg-block"></div>
        <h2 class="mb-6">MAIS COFORTO E COMODIDADE PARA A SUA VIDA</h2>
    </div>
  </div>

  <div class="row my-6">
    <div class="col-12">
      <div class="debito_description">
        <p>Clientes CIPA têm a opção do pagamento da cota condominial via Débito Automático.</p>

        <p>Faça o download do formulário, imprima-o, preencha todas as informações e deixe-o na portaria ou administração do
        seu condomínio para o recolhimento pelos motociclistas da Linha Direta. No prazo de 72 horas entraremos em contato.</p>

        <p>O Débito Automático CIPA atende aos bancos Bradesco, Itaú e Santander.</p>

        <p>Faça o download do formulário clicando no botão abaixo:</p>
      </div>

      <div class="debito_button mt-8">
        <a href="" class="btn btn-tsuru-blue rounded-pill p-3 py-md-4 px-md-5">preencher formulário <img src="assets/images/icones/arrow.svg" class="ml-4" alt=""></a>
      </div>
    </div>
  </div>

  <div class="row my-9">
    <div class="col-12">
      <div class="debito_obs p-5">
        <h3>Observações:</h3>
        <p>Se houver alguma cota condominial vencida e não paga, por favor regularize a situação para que o serviço seja disponibilizado.</p>
        <p>Na ausência de saldo suficiente na conta para pagamento da cota condominial, o pagamento deverá ser feito na CIPA, caso contrário
          o serviço poderá ser cancelado.+</p>
      </div>
    </div>
  </div>


</div>

</section>

<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
