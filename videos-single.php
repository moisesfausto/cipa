<?php require_once('header.php'); ?>
<?php

$subTitle = 'Conteúdo Exclusivo';
$title = 'Vídeos';
$description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum. Pellentesque vitae tellus ligula.';

?>
<?php include_once('widgets/page-title.php'); ?>

<div class="main_post_video">

  <div class="post_video_embed py-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="547" class="embed-responsive-item" src="https://www.youtube.com/embed/K_nC3DFlStA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="post_video_description py-5">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <time>26/10/2020</time>
          <h1>Administradora oferece mini mercado para os condomínios</h1>

          <p>Ut sit sit. Quidem omnis sed aut. Ut fugit consequatur exercitationem animi maiores iure non. Eos ullam dicta sapiente suscipit eos id odio. Labore delectus sed veritatis blanditiis voluptas.</p>

          <p>Aut non odit aliquid perspiciatis iusto. Similique nemo consequatur qui est sit. Quaerat dolores voluptatum excepturi tempore.</p>

          <p>Dolore rem ut et repellat deserunt occaecati. Provident repudiandae dolor rerum natus et sit labore a a. Odit est ipsa tenetur fugit repellendus vel voluptates veniam.</p>

          <p>Consequatur voluptas laboriosam impedit nisi repellendus. Esse blanditiis veniam. Nihil consequatur dicta dolorum quia sit.</p>

          <p>Velit maxime et expedita sit veritatis minus dolorem quo. Iste itaque est ut hic impedit nam nam sunt. Et inventore sunt adipisci porro quidem eos aliquam voluptatem ipsum. Ea minima vitae inventore maiores nostrum repellat eligendi. Distinctio eum consequuntur eaque. Dicta dicta tempora ut pariatur nihil doloribus adipisci impedit.</p>

          <p>Aut veritatis eum unde fuga nobis quia similique animi. Illum ut sunt aut cupiditate delectus et. Quibusdam facere eos corporis eum. Corporis sequi et ut repellendus neque ut ut expedita recusandae.</p>

          <p>Aut veritatis eum unde fuga nobis quia similique animi. Illum ut sunt aut cupiditate delectus et. Quibusdam facere eos corporis eum. Corporis sequi et ut repellendus neque ut ut expedita recusandae.</p>

          <p>Aut veritatis eum unde fuga nobis quia similique animi. Illum ut sunt aut cupiditate delectus et. Quibusdam facere eos corporis eum. Corporis sequi et ut repellendus neque ut ut expedita recusandae.</p>

        </div>
      </div>
    </div>
  </div>

  <!-- Share and Comments -->
  <div class="container">
    <div class="row">
      <div class="col-6">
        <div class="post_video_share">
          <span>Compartilhar:</span>
          <ul>
            <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook-square"></i></a></li>
            <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter-square"></i></a></li>
            <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="col-6 text-right">
        <div class="post_video_comments_number">
          <img src="assets/images/icones/comments.svg" alt="Comentarios">
          <span>2 Comentários</span>
        </div>
      </div>
    </div>
  </div>
  <!-- /End -->

  <!-- Main Comments -->
  <div class="main_comments">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="title_comments">
            <h2>comentários</h2><span></span>
          </div>
        </div>
      </div>

      <!-- Comments -->
      <div class="row">
        <div class="col-md-2">
          <div class="profile_comments mt-5">
            <img src="assets/images/img-testimony.jpg" alt="">
          </div>
        </div>
        <div class="col-md-10">
          <div class="name_comments mt-5 mb-3">Pablo Lucca Ribeiro</div>
          <time>26/10/2020</time>
          <div class="comment_comments mt-4 mb-5">Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</div>
        </div>
      </div>
      <!-- /End -->

      <!-- Comments Form -->
      <div class="row">
        <div class="col-12">
          <div class="title_comments">
            <div class="col-md-3"><h2>deixe seu comentário</h2></div><span></span>
          </div>
        </div>
      </div>

      <div class="form_comments my-5">
        <div class="row">
          <div class="col-12">
            <form action="">
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <input class="form-control form-control-lg rounded-pill" type="text" name="name" id="name" placeholder="Nome Completo">
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <input class="form-control form-control-lg rounded-pill" type="email" name="email" id="email" placeholder="E-mail">
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <textarea class="form-control" name="message" id="message" rows="3" placeholder="Comentário"></textarea>
                  </div>
                </div>
              </div>
              <div class="d-flex justify-content-end">
                <a href="" class="btn btn-tsuru-blue-escuro shadow rounded-pill py-3 px-5 font-weight-bolder">enviar</a>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /Ends -->
    </div>
  </div>
  <!-- /End -->

  <div class="main_related_post">

    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="title_comments">
            <div class="col-md-3"><h2>vídeos relacionados</h2></div><span></span>
          </div>
        </div>
      </div>

      <div class="main_videos">
        <div class="row mt-5">
          <div class="col-lg-12 mb-5">
            <div class="box_video">
              <div class="row">
                <div class="col-12 col-lg-6">
                  <div class="box_video_thumb">
                    <img src="assets/images/video02.png" class="img-fluid" alt="">
                    <div class="carousel-caption d-flex justify-content-center align-items-center">
                      <a href="" class="btn btn-light rounded-pill px-5 py-2">assistir vídeo <img
                          src="assets/images/icones/arrow-blue.svg" alt="" class="ml-3"></a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
                  <div class="box_video_text px-5 py-3 py-md-0">
                    <h3>Administradora oferece mini mercado para os condomínios</h3>
                    <p>Para minimizar o impacto da Pandemia nos condomínios, a Cipa oferece o Combo Bem Estar ...</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-12 mb-5">
            <div class="box_video">
              <div class="row">
                <div class="col-12 col-lg-6">
                  <div class="box_video_thumb">
                    <img src="assets/images/video02.png" class="img-fluid" alt="">
                    <div class="carousel-caption d-flex justify-content-center align-items-center">
                      <a href="" class="btn btn-light rounded-pill px-5 py-2">assistir vídeo <img
                          src="assets/images/icones/arrow-blue.svg" alt="" class="ml-3"></a>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
                  <div class="box_video_text px-5 py-3 py-md-0">
                    <h3>Administradora oferece mini mercado para os condomínios</h3>
                    <p>Para minimizar o impacto da Pandemia nos condomínios, a Cipa oferece o Combo Bem Estar ...</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<section class="main_see_too my-5">
  <div class="container">

    <div class="line bg-tsuru-red mx-auto mb-3"></div>
    <h2 class="text-center mb-5">Veja <span> Também</span></h2>

    <div class="row">

      <div class="col-lg-6 mb-4 mb-lg-0">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/ebooks.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <h3>E-Books</h3>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/cipa-na-midia.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <h1>Cipa na Mídia</h1>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>




<?php require_once('widgets/optin.php'); ?>
<?php require_once('footer.php'); ?>
