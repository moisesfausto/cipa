<?php include_once('header.php'); ?>

<?php

$subTitle = 'Contato';
$title = 'Formulário de Contato';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<section class="main_duvidas mt-8">


  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5">
        <div class="title_bg_gray d-none d-lg-block"></div>
        <h2 class="mb-6">DÚVIDAS FREQUENTES</h2>
      </div>
    </div>
  </div>

  <div class="duvidas_accordion mx-auto">
    <div class="accordion" id="">

      <?php for ($i=0; $i < 6 ; $i++): ?>
      <!-- Accordion #<?= $i ?> -->
      <div class="card mt-4">
        <div class="card-header" id="question">
          <div class="container">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left btn-outline-light text-decoration-none shadow-none <?= $i != 0 ? 'collapsed' : '' ?>" type="button" data-toggle="collapse" data-target="#collapse<?= $i ?>" aria-expanded="<?= $i == 0 ? 'true' : 'false' ?>" aria-controls="">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum?
              <img src="assets/images/icones/arrow-blue.svg" class="ml-6" alt="">
            </button>
          </h2>
          </div>
        </div>

        <div id="collapse<?= $i ?>" class="collapse answer" aria-labelledby="" data-answer>
          <div class="container">
            <div class="card-body py-5">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum. Pellentesque vitae tellus ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum. Pellentesque vitae tellus ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum.
            </div>
          </div>
        </div>
      </div>
      <!-- /End -->
      <?php endfor; ?>

    </div>
  </div>

</section>

<section class="main_contato">
  <div class="container">
    <div class="row">

      <div class="col-12 col-md-4">
        <div class="title_bg_gray d-none d-lg-block"></div>
        <h2 class="mb-6">CENTRAL DE ATENDIMENTO</h2>

        <p>A CIPA disponibiliza um canal exclusivo com
        os clientes. Por meio da ouvidoria, cliente
        CIPA pode tirar suas dúvidas, fazer elogiou
        ou reclamações.</p>

        <p>Se quiser solicitar uma proposta, escolha
        o tipo que deseja abaixo:</p>

        <p>Condomínios  |  Locações</p>

        <h3 class="mt-6">TELEFONE</h3>
        <span class="contato_telefone">+55 21 2196-5000</span>

        <h3 class="mt-5">HORÁRIO</h3>
        <span>Das 8h30 às 17h30</span>

        <h3 class="mt-5">LOCALIZAÇÃO</h3>
        <span>Rua México, 41 – 2o andar Centro – Rio de Janeiro – RJ CEP 20031-905</span>
      </div>

      <div class="col-md-1"></div>

      <div class="col-12 col-md-7 mt-8 mt-md-0">
        <h3 class="mb-6">FORMULÁRIO DE CONTATO</h3>
        <form action="">

          <div class="form-group">
            <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="name" id="name" placeholder="Nome Completo" required>
          </div>

          <div class="form-group">
            <input class="form-control form-control-lg rounded-pill btn-outline-light" type="email" name="email" id="email" placeholder="E-mail" required>
          </div>

          <div class="form-group">
            <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="subject" id="subject" placeholder="Assunto" required>
          </div>

          <div class="form-group">
            <textarea class="form-control btn-outline-light" name="message" id="message" rows="3" placeholder="Mensagem"></textarea>
          </div>

          <div class="col-12 d-flex justify-content-end">
            <button type="submit" class="btn btn-tsuru-blue-escuro font-weight-bolder shadow rounded-pill py-3 px-5 mt-5">enviar</button>
          </div>

        </form>
      </div>

    </div>
  </div>
</section>


<section class="main_map mt-8">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2185.2010018009514!2d-43.175147210769!3d-22.911328177649764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9981df5c04d0cb%3A0x7ecd0cd6875e6da3!2sR.%20M%C3%A9xico%2C%2041%20-%202o%20andar%20-%20Centro%2C%20Rio%20de%20Janeiro%20-%20RJ%2C%2020031-144!5e0!3m2!1spt-BR!2sbr!4v1607712817016!5m2!1spt-BR!2sbr" width="100%" height="650" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>

<?php require_once('widgets/optin.php'); ?>
<?php include_once('footer.php'); ?>
