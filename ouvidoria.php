<?php include_once('header.php'); ?>

<?php

$subTitle = 'Cliente Cipa';
$title = 'Ouvidoria';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<section class="main_ouvidoria mb-9">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-4">
        <div class="central_atendimento">

          <div class="title_bg_gray"></div>
          <h2 class="mb-6">CENTRAL DE ATENDIMENTO</h2>

          <p>CIPA disponibiliza um canal exclusivo com
          os clientes. Por meio da ouvidoria, cliente
          CIPA pode tirar suas dúvidas, fazer elogiou
          ou reclamações.</p>

          <p>Se quiser solicitar uma proposta, escolha
          o tipo que deseja abaixo:</p>

          <p>Condomínios  |  Locações</p>

          <h2 class="mt-6 mb-3">TELEFONE</h2>
          <span>+55 21 2196-5000</span>
        </div>
      </div>
      <div class="col-1"></div>
      <div class="col-12 col-md-7">
        <h2 class="mb-6 mt-5 mt-md-0">FORMULÁRIO DE CONTATO</h2>

        <form action="" class="form_ouvidoria">
          <div class="form-group">
            <input type="text" name="name" id="name" class="form-control form-control-lg rounded-pill btn-outline-light mb-4" placeholder="Nome Completo">
          </div>
          <div class="form-group">
            <input type="email" name="email" id="email" class="form-control form-control-lg rounded-pill btn-outline-light mb-4" placeholder="E-mail">
          </div>
          <div class="form-group">
            <input type="text" name="subject" id="subject" class="form-control form-control-lg rounded-pill btn-outline-light mb-4" placeholder="Assunto">
          </div>
          <div class="form-group">
            <textarea name="message" id="message" class="form-control mb-4" placeholder="Mensagem"></textarea>
          </div>
          <div class="d-flex justify-content-end">
            <a href="" class="btn btn-tsuru-blue-escuro  shadow rounded-pill py-3 px-5 font-weight-bolder">enviar</a>
          </div>
        </form>

      </div>

    </div>
  </div>
</section>

<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
