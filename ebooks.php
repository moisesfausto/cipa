<?php include_once('header.php'); ?>

<?php

$subTitle = 'Conteúdo Exclusivo';
$title = 'E-Books';
$description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum. Pellentesque vitae tellus ligula.';

?>
<?php include_once('widgets/page-title.php'); ?>

<section class="main_list_ebooks">
  <div class="container">
    <div class="row">

      <?php for ($i=0; $i < 12; $i++): ?>
      <div class="col-md-6 col-lg-3 mb-5">
        <div class="main_ebook text-center text-md-left">
          <a href="#" target="_blank"><img src="assets/images/ebook01.png" alt="..." ></a>
          <div class="main_ebook_content my-5">
            <time class="ebook_date_post text-center text-md-left">26/10/2020</time>
            <a href="#" target="_blank" class="text-decoration-none"><h2 class="text-center text-md-left">Curabitur facilisis sollicitudin tortor et elementum pellentesque vitae.</h2></a>
          </div>
        </div>
      </div>
      <?php endfor; ?>

    </div>
  </div>
</section>

<section class="main_see_too mt-5 mb-9">
  <div class="container">

    <div class="line bg-tsuru-red mx-auto mb-3"></div>
    <h2 class="text-center mb-5">Veja <span> Também</span></h2>

    <div class="row">

      <div class="col-lg-6 mb-4 mb-lg-0">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/cipa-na-midia.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <a href="" class="text-decoration-none"><h3>Cipa na Mídia</h3></a>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/videos.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <a href="" class="text-decoration-none"><h3>Vídeos</h3></a>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>

<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
