    <?php require_once('header.php'); ?>


    <section class="main_slide">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets/images/slide/slide03.png" class="d-block w-100 img-fluid" alt="...">
                    <div class="carousel-caption text-right d-flex flex-column align-items-end justify-content-center">

                        <h5>Muito mais <br> serviços para o <br> seu condomínio</h5>
                        <p>Por um valor que cabe no seu orçamento</p>
                        <a href="" class="btn btn-tsuru-blue btn-sm-lg rounded-pill shadow font-weight-bolder py-sm-3 px-sm-5 mt-sm-4" role="button">saiba mais</a>


                        <div class="request_proposal rounded-pill">
                            <a href="" class="btn btn-tsuru-blue btn-lg rounded-pill font-weight-bolder" role="button">Solicitar Proposta</a>
                            <img src="assets/images/icones/arrow.svg" alt="">
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </section>

    <main class="main_home">

        <section class="main_condominium">
            <div class="container">
                <div class="row">
                    <div class="main_condominium_content main_condominium_bg">
                        <div class="col-md-6">
                            <div class="line bg-tsuru-red mb-3"></div>
                            <h3 class="mb-4">Administração de condomínios</h3>
                            <h2 class="mb-4">Gestão Financeira Completa e <span>Digital</span></h2>
                            <p>Com assessoria dedicada e personalizada, cliente CIPA tem todas as facilidades para uma administração condominial com tranquilidade e confiança: gestão financeira completa e digital, previsão orçamentária, jurídico ativo e muitos diferenciais, como o cartão de crédito do condomínio e assessoria direta.</p>
                            <a href="">solicite um consultor <img src="assets/images/icones/arrow.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main_cipa-sindica">
            <div class="container">
                <div class="row">
                    <div class="main_cipa-sindica_content main_cipa-sindica_bg d-flex align-items-center">
                        <div class="d-flex justify-content-end mt-5">
                            <div class="col-md-6">
                                <h3 class="mb-2">Cipa Síndica + Gestão Operacional</h3>
                                <h2 class="mb-5">Serviço sob medida em que o gestor CIPA controla toda a operação do dia a dia</h2>
                                <p class="my-5">Equipe, manutenção e obras, com gestão integrada e transparente e proporcionando mais liberdade para o síndico tratar de assuntos que precisam mais da sua atenção no condomínio.</p>
                                <a class="btn btn-tsuru-red btn-lg rounded-pill py-3 px-5" href="#" role="button">solicitar uma proposta <img src="assets/images/icones/arrow.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main_install_cipa d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="main_install_cipa_content">
                        <div class="d-flex justify-content-end">
                            <div class="col-md-5">
                                <div class="line bg-tsuru-blue mb-3"></div>
                                <h3>Instale</h3>
                                <h2 class="mb-5">cipa</h2>
                                <p class="mb-5">Para gerir seu condomínio de forma integrada, com muito mais eficiência e facilidades para seu dia a dia, a CIPA tem as soluções ideais:</p>
                                <a class="btn btn-tsuru-blue btn-lg rounded-pill py-3 px-5" href="#" role="button">solicitar uma proposta <img src="assets/images/icones/arrow.svg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main_self-management">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <div class="line bg-tsuru-red mb-3 mt-5"></div>
                        <h2 class="mb-4">Auto <span>Gestão</span></h2>
                        <h3 class="mb-4">Você recebe todas as facilidades para uma administração condominial com transparência e confiança</h3>
                        <p class="mb-5">Gestão financeira completa e digital, previsão orçamentária, cobrança jurídica a devedores e diferenciais, como o cartão de crédito do condomínio** e assessoria especializada para o eSocial e a EFD-Reinf. Além disso, com o app CIPA Condomínios, síndicos e condôminos têm acesso a funcionalidades como 2ª via de boletos, extratos, aviso de encomendas, dentre outras.</p>
                    </div>
                </div>
                <div class="row justify-content-center my-5">
                    <div class="col-md-3">
                        <div class="facilities">
                            <div class="facilities_icone">
                                <img src="assets/images/icones/icone-01.svg" alt="Envio de Boleto">
                            </div>
                            <p>Envio de boletos de cota condominial direto da sua conta bancária ou da sua conta na CIPA</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-02.svg" alt="Pagamento de Fornecedores">
                        </div>
                            <p>Pagamento de fornecedores</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-03.svg" alt="Envio de Recebimentos de Boletos">
                        </div>
                            <p>Envio e recebimento de boletos em formato digital</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-04.svg" alt="Malote sob demanda">
                        </div>
                            <p>Malote sob demanda</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-05.svg" alt="Cobrança extra judicial">
                        </div>
                            <p>Cobrança extrajudicial a inadimplentes sem custos ao condomínio</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-06.svg" alt="Site e app em tempo real">
                        </div>
                            <p>Site e APP em tempo real</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="facilities">
                        <div class="facilities_icone">
                            <img src="assets/images/icones/icone-07.svg" alt="Prestação de contas mensal">
                        </div>
                            <p>Prestação de contas mensal, com total transparência</p>
                        </div>
                    </div>
                </div>
                <div class="attention">
                    <p>* Valor válido para condomínios de até 20 unidades e sem funcionários. Valor sem impostos. <br> ** Sujeito a análise.</p>
                    <span>Outros planos sob consulta</span>
                </div>
            </div>
        </section>
        <section class="main_app my-5">
            <div class="container">
                <div class="row">
                    <div class="main_app_content d-flex align-items-center py-5 py-sm-0">
                        <div class="d-flex justify-content-start">
                            <div class="col-md-6">
                                <div class="line bg-white mb-3"></div>
                                <h3 class="mb-2">Apps para condomínios</h3>
                                <p class="my-5">A CIPA oferece soluções imobiliárias completas e adequadas às necessidades de cada cliente, com a confiança que só uma administradora que alia tecnologia e experiência pode proporcionar.</p>
                                <a class="btn btn-tsuru-blue btn-lg rounded-pill py-3 px-5" href="#" role="button">baixe o aplicativo cipa condomínios</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main_summary_post my-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <div class="line bg-tsuru-red mb-3 mt-5"></div>
                        <h2 class="mb-3">Condomínio etc</h2>
                        <h3 class="mb-4">Receba todas as novidades e dicas Cipa.</h3>
                    </div>
                </div>
                <div class="last_post my-5">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-4 mb-md-0">
                            <div class="last_post_single_post">
                                <a href="" class="text-decoration-none"><img src="assets/images/post01.jpg" alt=""></a>
                                <a href="" class="text-decoration-none"><h2 class="my-4">Título da matéria em até duas ou mais linhas para leitura.</h2></a>
                                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-4 mb-md-0">
                            <div class="last_post_single_post">
                                <a href="" class="text-decoration-none"><img src="assets/images/post01.jpg" alt=""></a>
                                <a href="" class="text-decoration-none"><h2 class="my-4">Título da matéria em até duas ou mais linhas para leitura.</h2></a>
                                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-4 mb-md-0">
                            <div class="last_post_single_post">
                                <a href="" class="text-decoration-none"><img src="assets/images/post01.jpg" alt=""></a>
                                <a href="" class="text-decoration-none"><h2 class="my-4">Título da matéria em até duas ou mais linhas para leitura.</h2></a>
                                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-4 mb-md-0">
                            <div class="last_post_single_post">
                                <a href="" class="text-decoration-none"><img src="assets/images/post01.jpg" alt=""></a>
                                <a href="" class="text-decoration-none"><h2 class="my-4">Título da matéria em até duas ou mais linhas para leitura.</h2></a>
                                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
                            </div>
                        </div>


                    </div>
                </div>

                <a class="btn btn-tsuru-blue btn-lg rounded-pill py-3 px-5 mt-5" href="#" role="button">ver todos</a>

            </div>
        </section>
        <section class="main_testimony my-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <div class="line bg-tsuru-red mb-3 mt-5"></div>
                        <h2 class="mb-3">Depoimentos</h2>
                        <h3 class="mb-4">Você recebe todas as facilidades para uma administração condominial com transparência e confiança</h3>
                    </div>
                </div>
                <div class="last_testimony mt-5">
                    <div class="row">
                        <div class="col-md-12 col-lg-4 mb-4 mb-md-4">
                            <div class="last_testimony_single_testimony">
                                <div class="profile d-flex align-items-center">
                                    <img src="https://thispersondoesnotexist.com/image" alt="">
                                    <div class="details">
                                        <span class="name">Pablo Lucca Ribeiro</span>
                                        <span class="office">Sindico do Place Torre</span>
                                    </div>
                                </div>
                                <p>
                                    Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-4 mb-4 mb-md-4">
                            <div class="last_testimony_single_testimony">
                                <div class="profile d-flex align-items-center">
                                    <img src="https://thispersondoesnotexist.com/image" alt="">
                                    <div class="details">
                                        <span class="name">Pablo Lucca Ribeiro</span>
                                        <span class="office">Sindico do Place Torre</span>
                                    </div>
                                </div>
                                <p>
                                    Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.
                                </p>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-4 mb-4 mb-md-4">
                            <div class="last_testimony_single_testimony">
                                <div class="profile d-flex align-items-center">
                                    <img src="https://thispersondoesnotexist.com/image" alt="" class="rounded-circle">
                                    <div class="details">
                                        <span class="name">Pablo Lucca Ribeiro</span>
                                        <span class="office">Sindico do Place Torre</span>
                                    </div>
                                </div>
                                <p>
                                    Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="btn btn-tsuru-blue btn-lg rounded-pill py-3 px-5 mt-5" href="#" role="button">ver todos</a>

            </div>
        </section>

    </main>

<?php require_once('footer.php'); ?>

