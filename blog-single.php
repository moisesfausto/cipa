<?php require_once('header.php'); ?>
<?php

$subTitle = 'Blog Condomínio';
$title = 'Blog';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<main class="main_single_post">
  <section class="single_post_cover d-flex align-items-center" style="background-image: url(assets/images/cover-post-blog.jpg);">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <span class="badge badge-tsuru-blue badge-category mb-4">oportunidade</span>
          <h1>Administradora oferece mini mercado para os condomínios</h1>
          <div class="single_post_date_comments d-flex align-items-end">
            <div class="single_post_date mr-4">
              <time>26/10/2020</time>
            </div>
            <div class="single_post_comments">
              <img src="assets/images/icones/comments.svg" alt="Comentarios">
              <span>2 Comentários</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="single_post_content">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-8">
          <!-- Banner -->
          <div class="post_content_banner">
            <img src="https://via.placeholder.com/770x198" class="img-fluid" alt="">
          </div>

          <!-- Player -->
          <div class="post_content_player my-5">
            <img src="assets/images/Aúdio.png" class="img-fluid" alt="">
          </div>

          <article>
            <h2>Dolorem ducimus nobis</h2>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint numquam ea ut voluptates quis. Amet, itaque autem! Soluta consequuntur quia rerum, dolor voluptatibus quae quasi, dolore aut optio sunt qui!</p>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint numquam ea ut voluptates quis. Amet, itaque autem! Soluta consequuntur quia rerum, dolor voluptatibus quae quasi, dolore aut optio sunt qui!</p>
            <img src="https://via.placeholder.com/770x450" alt="">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint numquam ea ut voluptates quis. Amet, itaque autem! Soluta consequuntur quia rerum, dolor voluptatibus quae quasi, dolore aut optio sunt qui!</p>
            <h2>Labore velit qui</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa quos sit accusantium adipisci earum itaque mollitia officiis repellendus ipsam molestiae, deserunt vitae atque iusto. Officiis enim ipsum accusantium magnam minima!</p>
            <h3>distinctio nostrum nihil</h3>
            <ul>
              <li>Lorem, ipsum dolor.</li>
              <li>Lorem, ipsum dolor.</li>
              <li>Lorem, ipsum.</li>
              <li>Lorem, ipsum.</li>
            </ul>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis doloribus, ipsam excepturi beatae pariatur voluptate?</p>
            <h4>Non dolorem dignissimos</h4>
            <a href="http://" target="_blank" rel="noopener noreferrer">Lorem ipsum dolor sit.</a>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius, sint!</p>
          </article>

          <!-- Share and Comments -->
          <div class="post_content_share_comments">
            <div class="row">
              <div class="col-6">
                <div class="post_video_share">
                  <span>Compartilhar:</span>
                  <ul>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter-square"></i></a></li>
                    <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-6 text-right">
                <div class="post_video_comments_number">
                  <img src="assets/images/icones/comments.svg" alt="Comentarios">
                  <span>2 Comentários</span>
                </div>
              </div>
            </div>
          </div>
          <!-- /End -->

          <!-- Banner -->
          <div class="post_content_banner mt-5">
            <img src="https://via.placeholder.com/770x198" class="img-fluid" alt="">
          </div>

          <!-- Post Comments -->
          <div class="row">
            <div class="col-12">
              <div class="title_comments">
                <h2>comentários</h2><span></span>
              </div>
            </div>
          </div>

          <!-- Comments -->
          <div class="row">
            <div class="col-md-3">
              <div class="profile_comments mt-5">
                <img src="assets/images/img-testimony.jpg" alt="">
              </div>
            </div>
            <div class="col-md-9">
              <div class="name_comments mt-5 mb-3">Pablo Lucca Ribeiro</div>
              <time>26/10/2020</time>
              <div class="comment_comments mt-4 mb-5">Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</div>
            </div>
          </div>

          <!-- Comments Form -->
          <div class="row">
            <div class="col-12">
              <div class="title_comments">
                <div class="col-md-3"><h2>deixe seu comentário</h2></div><span></span>
              </div>
            </div>
          </div>

          <div class="form_comments my-5">
            <div class="row">
              <div class="col-12">
                <form action="">
                  <div class="row">
                    <div class="col-12">
                      <div class="form-group">
                        <input class="form-control form-control-lg rounded-pill" type="text" name="name" id="name" placeholder="Nome Completo">
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <input class="form-control form-control-lg rounded-pill" type="email" name="email" id="email" placeholder="E-mail">
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <textarea class="form-control" name="message" id="message" rows="3" placeholder="Comentário"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="d-flex justify-content-end">
                    <a href="" class="btn btn-tsuru-blue-escuro shadow rounded-pill py-3 px-5 font-weight-bolder">enviar</a>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- Prev and Next Post -->
          <div class="post_content_prev_next">
            <div class="row">
              <div class="col-6 text-right d-flex align-items-end flex-column">
                <div class="post_content_line"></div><div class="line_prev"></div>
                <span class="text-uppercase">anterior</span>
                <h2 class="mt-5">O dia Online: Congresso Virtual</h2>
              </div>
              <div class="col-6 text-left d-flex align-items-start flex-column">
                <div class="post_content_line line_right"></div><div class="line_next"></div>
                <span class="text-uppercase next">próximo</span>
                <h2 class="mt-5">Administradora oferece mini mercado para os condomínios</h2>
              </div>
            </div>
          </div>

          <!-- Related Post -->
          <div class="row">
            <div class="col-12">
              <div class="title_comments">
                <div class="col-md-5"><h2>posts relacionados</h2></div><span class="ml-0"></span>
              </div>
            </div>
          </div>

          <div class="main_post_list my-5">
            <div class="row">

              <?php for ($i = 0; $i < 4; $i++): ?>
              <div class="col-12 col-lg-6 mb-5">
                <img src="assets/images/listagem-de-post.png" class="img-fluid" alt="">
                <h2 class="post_list_title mt-4 mb-3">Título da matéria em até duas ou mais linhas para leitura</h2>
                <p class="post_list_description">Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
              </div>
              <?php endfor; ?>

            </div>
          </div>




        </div>

        <!-- SideBar -->
        <div class="col-12 col-md-4 mt-5 mt-sm-0">
          <aside class="main_post_sidebar">

            <div class="main_form_search">
              <form action="">
                <div class="input-group">
                  <input type="text" class="form-control btn-outline-light" placeholder="O que você procura">
                  <div class="input-group-append">
                    <button class="btn btn-outline-light" type="button"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>

            <div class="post_sidebar_category mt-5">
              <span>Categorias</span>
              <ul class="mt-4">
                <li><a href="" class="text-decoration-none">Nome da Categoria 01</a></li>
                <li><a href="" class="text-decoration-none">Nome da Categoria 02</a></li>
                <li><a href="" class="text-decoration-none">Nome da Categoria 03</a></li>
                <li><a href="" class="text-decoration-none">Nome da Categoria 04</a></li>
                <li><a href="" class="text-decoration-none">Nome da Categoria 05</a></li>
              </ul>
            </div>
            <div class="post_sidebar_more_ready">
              <span>Os mais lidos</span>
              <ul class="mt-5">
                <li><span>1</span>O dia Online: Congresso Virtual</li>
                <li><span>1</span>Administradora oferece mini mercado para os condomínios</li>
                <li><span>1</span>O dia Online: Congresso Virtual</li>
                <li><span>1</span>Administradora oferece mini mercado para os condomínios</li>
                <li><span>1</span>O dia Online: Congresso Virtual</li>
              </ul>
            </div>

            <div class="post_sidebar_banner">
              <img src="https://via.placeholder.com/370" class="img-fluid" alt="">
            </div>
          </aside>
        </div>

      </div>
    </div>
  </section>




</main>

<?php require_once('widgets/optin.php'); ?>
<?php require_once('footer.php'); ?>
