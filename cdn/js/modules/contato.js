export default function initPageContato()
{

  function addClass(question, index) {

    const answer = document.querySelector('#collapse'+index);

    if (!answer.classList.contains('show')) {
      question.classList.add('show_display', 'rotate-show');
    } else {
      question.classList.remove('show_display', 'rotate-show');
    }

  }

  const questions = document.querySelectorAll('#question');

  if( questions ) {
    questions.forEach( (question, index) => {
      question.addEventListener('click', () => {
        addClass(question, index);
      });
    });
  }


}
