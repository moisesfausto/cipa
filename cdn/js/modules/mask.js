function initMask(...inputs) {

  function fMasc(objeto, mascara) {
    const obj = objeto;
    const masc = mascara;
    setTimeout( fMascEx(obj, masc), 1);
  }

  function fMascEx(obj, masc) {
    //obj.value = masc(obj.value);
    if (masc === 'maskCPF') {
      return obj.value = maskCPF(obj.value);
    }
    if (masc === 'maskCNPJ') {
      return obj.value = maskCNPJ(obj.value);
    }
    if (masc === 'maskPHONE') {
      return obj.value = maskPHONE(obj.value);
    }
    if (masc === 'maskDATA') {
      return obj.value = maskDATA(obj.value);
    }

  }

  function maskCNPJ(cnpj){

    cnpj = cnpj.replace(/\D/g, "");
    cnpj = cnpj.replace(/^(\d{2})(\d)/, "$1.$2");
    cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
    cnpj = cnpj.replace(/\.(\d{3})(\d)/, ".$1/$2");
    cnpj = cnpj.replace(/(\d{4})(\d)/, "$1-$2");
    return cnpj;

  }

  function maskCPF(cpf) {
    cpf = cpf.replace(/\D/g, "");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
    cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
    return cpf;

  }

  function maskPHONE(tel) {

    tel = tel.replace(/\D/g, "");
    tel = tel.replace(/^(\d)/, "($1");
    tel = tel.replace(/(.{3})(\d)/, "$1)$2");
    if(tel.length == 9) {
      tel = tel.replace(/(.{1})$/, "-$1");
    } else if (tel.length == 10) {
      tel = tel.replace(/(.{2})$/, "-$1");
    } else if (tel.length == 11) {
      tel = tel.replace(/(.{3})$/, "-$1");
    } else if (tel.length == 12) {
      tel = tel.replace(/(.{4})$/, "-$1");
    } else if (tel.length > 12) {
      tel = tel.replace(/(.{4})$/, "-$1");
    }
    return tel;

  }

  function maskDATA(data){

    data = data.replace(/\D/g,"");
    data = data.replace(/(\d{2})(\d)/,"$1/$2");
    data = data.replace(/(\d{2})(\d)/,"$1/$2");
    data = data.replace(/(\d{2})(\d{2})$/,"$1$2");
    return data;

  }


  inputs.forEach( (input) => {
    let maskType = input.getAttribute('data-mask');
    input.addEventListener('keydown', () => { fMasc(input, maskType) });
  });


}
