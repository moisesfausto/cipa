import initMask from './mask.js';

export default function initClickRadio() {
  function clickRadio(escolha) {

    const entidade = escolha.getAttribute('data-entity');
    const cpf = document.querySelector('#cpf');
    const cnpj = document.querySelector('#cnpj');

    if (entidade === 'pf') {

      cnpj.setAttribute('disabled', 'disabled');

      if(cpf.hasAttribute('disabled', 'disabled')){
        cpf.removeAttribute('disabled', 'disabled')
      }

    } else if(entidade === 'pj') {

      cpf.setAttribute('disabled', 'disabled');

      if (cnpj.removeAttribute('disabled', 'disabled')){
        cnpj.removeAttribute('disabled', 'disabled')
      }

    }

  }

  const tipoPessoa = document.querySelectorAll('input[type="radio"]');

  if (tipoPessoa) {
    tipoPessoa.forEach( (escolha) => {

      escolha.addEventListener('click', () => {
        clickRadio(escolha);
      });

    });
  }

  const cpf = document.querySelector('#cpf');
  const cnpj = document.querySelector('#cnpj');
  const phone = document.querySelector('#phone');

  //initMask(cpf, cnpj, phone);

}





