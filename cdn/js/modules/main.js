export default function initScriptHome(){

    function show_menu_account(){
        if ( !account.classList.contains('active') ) {
            account.classList.add('active');
            buttons = document.querySelectorAll('.account a');
            buttons.forEach( (button) => {
                if ( button.classList.contains('d-none;') ){
                    button.classList.remove('d-none')
                }else{
                    button.classList.add('d-none');
                }
            });

        } else {
            account.classList.remove('active')
            buttons = document.querySelectorAll('.account a')
            buttons.forEach( button => {
                if ( button.classList.contains('d-none') ) {
                    button.classList.remove('d-none');
                } else {
                    button.classList.add('d-none');
                }
            });
        }
    }

    function show_menu_services(){
        if ( !services.classList.contains('active') ) {
            services.classList.add('active');

            buttons = document.querySelectorAll('.services a');
            buttons.forEach( button => {
                if ( button.classList.contains('d-none') ){
                    button.classList.remove('d-none');
                } else {
                    button.classList.add('d-none');
                }
            });
        } else {
            services.classList.remove('active')
            buttons = document.querySelectorAll('.services a');
            buttons.forEach( button => {
                if ( button.classList.contains('d-none') ) {
                    button.classList.remove('d-none');
                }else{
                    button.classList.add('d-none');
                }
            });
        }
    }

    const account = document.querySelector('.account');
    const services = document.querySelector('.services');

    if ( account && services ) {
        account.addEventListener('click', show_menu_account);
        services.addEventListener('click', show_menu_services);
    }
}

