function initHome() {

  function show_menu_account(){
    if ( !account.classList.contains('active') ) {
        account.classList.add('active');
        buttons = document.querySelectorAll('.account a');
        buttons.forEach( (button) => {
            if ( button.classList.contains('d-none;') ){
                button.classList.remove('d-none')
            }else{
                button.classList.add('d-none');
            }
        });

    } else {
        account.classList.remove('active')
        buttons = document.querySelectorAll('.account a')
        buttons.forEach( button => {
            if ( button.classList.contains('d-none') ) {
                button.classList.remove('d-none');
            } else {
                button.classList.add('d-none');
            }
        });
    }
  }

  function show_menu_services(){
      if ( !services.classList.contains('active') ) {
          services.classList.add('active');

          buttons = document.querySelectorAll('.services a');
          buttons.forEach( button => {
              if ( button.classList.contains('d-none') ){
                  button.classList.remove('d-none');
              } else {
                  button.classList.add('d-none');
              }
          });
      } else {
          services.classList.remove('active')
          buttons = document.querySelectorAll('.services a');
          buttons.forEach( button => {
              if ( button.classList.contains('d-none') ) {
                  button.classList.remove('d-none');
              }else{
                  button.classList.add('d-none');
              }
          });
      }
  }

  const account = document.querySelector('.account');
  const services = document.querySelector('.services');

  if ( account && services ) {
      account.addEventListener('click', show_menu_account);
      services.addEventListener('click', show_menu_services);
  }

  $('.carousel').carousel()
}

function initCadastro(){

  function clickRadio(escolha) {

    const entidade = escolha.getAttribute('data-entity');
    const cpf = document.querySelector('#cpf');
    const cnpj = document.querySelector('#cnpj');

    if (entidade === 'pf') {

      cnpj.setAttribute('disabled', 'disabled');

      if(cpf.hasAttribute('disabled', 'disabled')){
        cpf.removeAttribute('disabled', 'disabled')
      }

    } else if(entidade === 'pj') {

      cpf.setAttribute('disabled', 'disabled');

      if (cnpj.removeAttribute('disabled', 'disabled')){
        cnpj.removeAttribute('disabled', 'disabled')
      }

    }

  }

  const tipoPessoa = document.querySelectorAll('input[type="radio"]');

  if (tipoPessoa) {
    tipoPessoa.forEach( (escolha) => {

      escolha.addEventListener('click', () => {
        clickRadio(escolha);
      });

    });
  }

  /**
   * Script de Mascara
   */

  const cpf = document.querySelector('#cpf');
  const cnpj = document.querySelector('#cnpj');
  const phone = document.querySelector('#phone');

  if(cpf && cnpj && phone){
    initMask(cpf, cnpj, phone);
  }

}


function initContato(){

  function addClass(question, index) {

    const answer = document.querySelector('#collapse'+index);

    if (!answer.classList.contains('show')) {
      question.classList.add('show_display', 'rotate-show');
    } else {
      question.classList.remove('show_display', 'rotate-show');
    }

  }

  const questions = document.querySelectorAll('#question');

  if( questions ) {
    questions.forEach( (question, index) => {
      question.addEventListener('click', () => {
        addClass(question, index);
      });
    });
  }

}


initHome();
initCadastro();
initContato();






