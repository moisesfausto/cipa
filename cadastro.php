<?php include_once('header.php'); ?>

<?php

$subTitle = 'Cliente Cipa';
$title = 'Atualize seu Cadastro';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>


<section class="main_atualizacao_cadastro mt-8">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
        <div class="title_bg_gray d-none d-lg-block"></div>
        <h2 class="mb-6">MAIS TRANQUILIDADE E SEGURANÇA</h2>
      </div>
    </div>
  </div>

  <div class="cadastro_info">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>Manter seus dados atualizados nos permite proporcionar mais tranquilidade e segurança para você.</p>
          <p>Todos os campos do formulário ao lado são de preenchimento obrigatório.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="cadastro_form mt-6">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <form action="">
            <div class="row">

              <div class="col-12">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="nameOwner" id="nameOwner" placeholder="Nome do Proprietário (Titular do Imóvel)" required>
                </div>
              </div>

              <div class="col-12 form_group_check">
                <div class="form-check form-check-inline">
                  <input class="form-check-input mx-3" type="radio" data-entity="pf" name="entity" id="pf" value="Pessoa Física">
                  <label class="form-check-label" for="pf">Pessoa Física</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input mx-3" type="radio" data-entity="pj" name="entity" id="pj" value="Pessoa Jurídica">
                  <label class="form-check-label" for="pj">Pessoa Jurídica</label>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="cpf" id="cpf" data-mask="maskCPF" placeholder="CPF (Apenas Números)" maxlength="14">
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="cnpj" id="cnpj" data-mask="maskCNPJ" placeholder="CNPJ (ex: XX-XXX-XXX/XXXX-XX)" maxlength="18">
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="phone" id="phone" data-mask="maskPHONE" placeholder="Telefone Residencial ou de Contato" required>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="email" name="email" id="email" placeholder="E-mail" required>
                </div>
              </div>

              <div class="col-12">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="address" id="address" placeholder="Endereço de Imóvel" required>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="complement" id="complement" placeholder="Complemento" required>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="district" id="district" placeholder="Baiiro" required>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="city" id="city" placeholder="Municipio" required>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="state" id="state" placeholder="Estado" required>
                </div>
              </div>

              <div class="col-12">
                <div class="form-group">
                  <input class="form-control form-control-lg rounded-pill btn-outline-light" type="text" name="nameCondominium" id="name" placeholder="Nome do Condomínio" required>
                </div>
              </div>

              <div class="col-12 d-flex justify-content-start">
                <button type="submit" class="btn btn-tsuru-blue shadow rounded-pill py-3 px-5 mt-5 font-weight-bolder">preencher formulário <img class="ml-3" src="assets/images/icones/arrow.svg" alt=""></button>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


</section>


<?php require_once('widgets/optin.php'); ?>
<?php include_once('footer.php'); ?>
