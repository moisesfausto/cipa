<?php include_once('header.php'); ?>

<?php

$subTitle = 'A empresa';
$title = 'Nossa História';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<main class="main_our_history">

  <section class="main_our_history_cover d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-5">
          <h1>Toda história de Sucesso, tem a sua marca.</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="main_our_history_content">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <p>Fundada em 23 de abril de 1954, a CIPA cresceu de forma exponencial e garantiu, ao longo de toda sua trajetória
          de
          sucesso, o reconhecimento de ser considerada sinônimo de qualidade, sustentabilidade e inovação em negócios
          imobiliários. Estar há tanto tempo, com solidez, nesse mercado, é consequência também da confiança e do foco
          constante
          em proporcionar experiências positivas para todos os clientes.</p>
          <p>A excelência em prestação de serviços e no relacionamento tem garantido à CIPA uma posição consolidada de
            referência no setor imobiliário e se refletido em seus
            negócios, muitos conquistados por meio da indicação dos clientes. </p>
          <p>E é graças a essa estratégia de posicionamento e com foco em pessoas, tecnologia e processos, que a CIPA gera seu
            crescimento a partir de seus próprios recursos, investindo
            constantemente em tecnologia para aperfeiçoar, modernizar e otimizar seus processos. Assim, consegue realizar seu
            planejamento estratégico de forma mais segura e eficaz. </p>
          <p>A CIPA se orgulha de ter uma longa história de comprometimento e construção de uma empresa sólida por todos os
            colaboradores que v</p>
          <p>Os constantes investimentos em tecnologia qualificada, captação de talentos e capacitação, sem dúvida, nos
            garantem vantagens competitivas em serviços, produtos,
            parcerias, novos negócios e foco em melhorias constantes no atendimento e no relacionamento com nossos clientes.
          </p>
          <p>Somos uma das maiores empresas nacionais do mercado imobiliário. Administramos mais de 1.300 condomínios e 68 mil
            imóveis, com
            cerca de 7.500 funcionários, 130 mil moradores e mais de 12 mil fornecedores atendidos. São mais de 37 mil seguros
            ativos entre as mais diversas coberturas. Além disso, nossa capacidade técnica, que garante segurança e satisfação
            aos
            clientes, nos permite controlar e processar mais de 3 milhões de informações ao mês. Esses números demonstram todo
            nosso
            compromisso com a excelência no atendimento e com a orientação para resultado, garantindo mais valor e retorno
            para cada
            investimento de nossos clientes.</p>
          <p>Somos também uma empresa certificada por diversas instituições, que atestam a nossa credibilidade e a qualidade
            dos serviços prestados.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="main_our_history_services">
    <div class="container">

      <!-- 1 Service -->
      <div class="row">
        <div class="col-lg-6 d-none d-md-block mb-4 mb-lg-0 text-center">
          <div class="service_cover cover_solucoes_especializadas"></div>
        </div>
        <div class="col-lg-6">
          <div class="service_description">
            <h2 class="mb-5">SOLUÇÕES ESPECIALIZADAS EM NEGÓCIOS IMOBILIÁRIOS</h2>
            <p>A CIPA oferece soluções imobiliárias completas e adequadas
              às necessidades de cada cliente, com a confiança que só
              uma administradora que alia
              tecnologia e experiência pode proporcionar.</p>
            <h3 class="my-4">CONDOMÍNIOS</h3>
            <p>Especialista em gestão de propriedades, a CIPA oferece
            serviços sob medida para condomínios, realizados por uma
            equipe de colaboradores
            preparados para lidar com toda a rotina das áreas da
            administração condominial.</p>
            <p>Para gerir seu condomínio de forma integrada, com muito
            mais eficiência e facilidades para seu dia a dia, a CIPA tem
            as soluções ideais:</p>
          </div>
        </div>
      </div>

      <!-- 2 Service -->
      <div class="row my-5">
        <div class="col-lg-5 d-flex align-items-center">
          <div class="service_description">
            <h2 class="mb-5">GESTÃO ADMINISTRATIVA E FINANCEIRA</h2>
            <p>Com assessoria dedicada e personalizada, cliente CIPA tem
              todas as facilidades para uma administração condominial
              com tranquilidade e confiança: gestão financeira completa
              e digital, previsão orçamentária, jurídico ativo e muitos
              diferenciais, como o cartão de crédito do condomínio e
              assessoria direta. Além disso, com o aplicativo CIPA
              Condomínios, síndicos e condôminos têm acesso a
              funcionalidades como 2ª via de boletos, extratos, aviso
              de encomendas, dentre outras.</p>
          </div>
        </div>
        <div class="col-lg-7 d-none d-md-block mb-4 mb-lg-0 text-center">
          <div class="service_cover cover_gestao_adm"></div>
        </div>
      </div>

      <!-- 3 Service -->
      <div class="row">
        <div class="col-lg-6 d-none d-md-block mb-4 mb-lg-0 text-center">
          <div class="service_cover cover_gestao_operacional"></div>
        </div>
        <div class="col-lg-6 d-flex align-items-center">
          <div class="service_description">
            <h2 class="my-5">SOLUÇÕES ESPECIALIZADAS EM NEGÓCIOS IMOBILIÁRIOS</h2>
            <p>Serviço sob medida em que o gestor CIPA controla toda a
              operação do dia a dia, como equipe, manutenção e obras,
              com gestão integrada e
              transparente e proporcionando mais liberdade para o
              síndico tratar de assuntos que precisam mais da sua
              atenção no condomínio.</p>
            <h2 class="my-5">CIPA SÍNDICA</h2>
            <p>Seu condomínio está com dificuldades para eleger um
              síndico ou deseja uma gestão profissional? Não precisa se
              preocupar: em conjunto com os membros do conselho, a
              CIPA Síndica exerce a sindicatura de seu condomínio,
              atendendo às necessidades específicas e assumindo o
              controle total da operação e de todos os serviços oferecidos
              aos condôminos.</p>
          </div>
        </div>
      </div>

      <!-- 4 Service -->
      <div class="row my-3 mt-md-10">
        <div class="col-lg-5 d-flex align-items-center">
          <div class="service_description">
            <h2 class="my-4">IMÓVEIS – LOCAÇÕES E VENDAS</h2>
            <p>A CIPA garante satisfação de expectativa de retorno e
              segurança para o investimento de seus clientes, graças à
              experiência, transparência e tradição em negócios
              imobiliários. Oferecemos serviços exclusivos para
              proporcionar tranquilidade e agilidade na hora de alugar,
              comprar ou vender um imóvel. Nossa consultoria
              especializada em investimentos imobiliários possibilita
              análise completa do melhor resultado para o seu patrimônio.
              </p>
            <h2 class="my-4">SEGUROS</h2>
            <p>A CIPA Corretora de Seguros é altamente preparada para
              assessorar os clientes na contratação de todos os tipos de
              seguros, como automóvel, residencial, saúde, consórcios, viagem,
              portáteis, empresarial, sempre buscando o melhor custo com as
              coberturas mais completas. Os seguros são oferecidos em
              parceria com seguradoras reconhecidas no mercado,
              garantindo a qualidade e a confiança que você procura.</p>
          </div>
        </div>
        <div class="col-lg-6 d-none d-md-flex justify-content-center flex-column mb-4 mb-lg-0 text-center">
          <div class="service_cover cover_imob"></div>
        </div>
      </div>


    </div>

  </section>



</main>

<?php require_once('widgets/optin.php'); ?>

<?php include_once('footer.php'); ?>
