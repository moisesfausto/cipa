<?php require_once('widgets/optin.php'); ?>

<footer>
    <nav class="main_footer_navbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-9">
                    <div class="row">
                        <div class="col-lg-3">
                            <ul>
                                <li>Conteúdo Gratuito</li>
                                <li><a href="ebooks.php">E-Books</a></li>
                                <li><a href="cipa-na-midia.php">Cipa na Mídia</a></li>
                                <li><a href="videos.php">Vídeos</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <ul>
                                <li>Condomínio etc</li>
                                <li><a href="categoria.php">Categorias</a></li>
                                <li><a href="blog.php">Blogpost</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <ul>
                                <li>A Empresa</li>
                                <li><a href="nossa-historia.php">Nossa História</a></li>
                                <li><a href="">Trabalhe Conosco</a></li>
                                <li><a href="">Área do Colaborador</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3">
                            <ul>
                                <li>Outros Serviços</li>
                                <li><a href="">Cipa Locação</a></li>
                                <li><a href="">Cipa Vendas</a></li>
                                <li><a href="">Cipa Corretora de Seguro</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <ul>
                        <li>Cliente Cipa</li>
                        <li><a href="boleto.php">2.º via de Boleto</li>
                        <li><a href="debito.php">Débito Automático</a></li>
                        <li><a href="cadastro.php">Atualização de Cadastro</a></li>
                        <li><a href="ouvidoria.php">Ouvidoria</a></li>
                        <li><a href="parceiros.php">Parceiros</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <section class="main_attendance my-5 d-flex justify-content-center">
        <div class="bg_attendance py-4 rounded-pill">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 mb-4 mb-md-0">
                        <div class="address">
                            <span class="title">Endereços</span>
                            <p class="my-2">Rua México, 41, 2º andar - Centro - Rio de Janeiro - RJ</p>
                            <p class="mb-2">Av. Nuta James, 65 - Barra da Tijuca - Rio de Janeiro - RJ - Condado dos Cascais</p>
                            <p class="mb-2">Rua México, 41, 2º andar - Centro - Cabo Frio - RJ</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 mb-md-0">
                        <div class="phone">
                            <span class="title">Atendimento</span>
                            <p>+55 21 2196 5000</p>
                            <span class="title">Atendimento Comercial</span>
                            <p>+55 21 2524-0553</p>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-3 mt-4 mt-lg-0">
                        <div class="social_media">
                            <span class="title">Redes Sociais</span>
                            <ul>
                                <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-facebook-square"></i></a></li>
                                <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter-square"></i></a></li>
                                <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-instagram-square"></i></a></li>
                                <li><a href="http://" target="_blank" rel="noopener noreferrer"><i class="fab fa-linkedin"></i></a></li>
                            </ul>
                            <a class="btn btn-tsuru-blue rounded-pill p-3 d-flex justify-content-between" href="#" role="button">Indique Condomínios <img src="assets/images/icones/arrow.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main_partner_certificates">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-2">
                    <div class="certificates">
                        <span class="title">Certificados</span>
                        <div class="certificates_logos mt-4">
                            <img src="assets/images/certificados/irem.png" alt="">
                            <img src="assets/images/certificados/procondo.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-10 mt-4 mt-lg-0">
                    <div class="partner">
                        <span class="title">Parceiros</span>
                        <div class="partner_logos mt-4">


                            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item d-flex justify-content-center align-items-center flex-row flex-wrap active" data-interval="2000">
                                        <img src="assets/images/parceiros/abadi.png" class="" alt="...">
                                        <img src="assets/images/parceiros/secovirio.png" class="" alt="...">
                                        <img src="assets/images/parceiros/ral.png" class="" alt="...">
                                        <img src="assets/images/parceiros/ademrj.png" class="" alt="...">
                                        <img src="assets/images/parceiros/brasil.png" class="" alt="...">
                                        <img src="assets/images/parceiros/fiabcibrasil.png" class="" alt="...">
                                    </div>
                                </div>
                                <a class="carousel-control-prev justify-content-start d-none d-sm-flex" href="#carouselExampleFade" role="button" data-slide="prev">
                                    <span class="" aria-hidden="true"><i class="fas fa-angle-left"></i></span>
                                </a>
                                <a class="carousel-control-next justify-content-end d-none d-sm-flex" href="#carouselExampleFade" role="button" data-slide="next">
                                    <span class="" aria-hidden="true"><i class="fas fa-angle-right"></i></span>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main_copyright mt-5 d-flex align-items-center">
        <div class="container">
            <div class="row justify-content-center justify-content-sm-between align-items-center">
                <p class="mb-3 mb-sm-0 mr-3">© 2020 - Direitos Reservados</p>
                <div class="developer_by d-flex flex-row">
                    <p class="mb-0">Desenvolvido por </p>
                    <a href="http://" target="_blank" rel="noopener noreferrer"><div class="logo_tsuru_footer ml-3"></div></a>
                </div>
            </div>
        </div>
    </section>
</footer>

<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="cdn/js/modules/mask.js"></script>
<script src="cdn/js/scripts.js"></script>

</body>
</html>
