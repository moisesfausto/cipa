<section class="main_newsletter d-flex align-items-center py-5 my-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <p>Cadastre-se em nossa newsletter e receba todas as novidades do Grupo Cipa em seu e-mail.</p>
      </div>
      <div class="col-lg-6">
        <form action="" class="">
          <div class="form-row">
            <div class="form-group col-md-6 mb-2">
              <input type="text" class="form-control rounded-pill" id="newsletter_name" placeholder="Nome">
            </div>
            <div class="form-group col-md-6 mb-2">
              <div>
                <input type="email" class="form-control rounded-pill" id="newsletter_email" placeholder="E-mail">
                <button class="btn btn-transparent" type="submit"><img src="assets/images/icones/arrow.svg"
                    alt=""></button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

  </div>
</section>
