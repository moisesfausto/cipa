<div class="main_page_inner">
  <div class="bg_page_title"></div>
  <div class="main_page_inner_title py-4">
    <div class="container">
      <div class="row flex-column">
        <div class="col-12">
          <div class="line bg-tsuru-red mb-3"></div>
          <p class="main_page_sub_title"><?= $subTitle ?></p>
          <h1 class="main_page_title"><?= $title ?></h1>
          <p class="main_page_description mt-4"><?= $description ?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="line_page_title"></div>
</div>
