<?php require_once('header.php'); ?>
<?php

$subTitle = 'Conteúdo Exclusivo';
$title = 'Vídeos';
$description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur facilisis sollicitudin tortor et elementum. Pellentesque vitae tellus ligula.';

?>
<?php include_once('widgets/page-title.php'); ?>


<section class="main_videos">
  <div class="container">

    <div class="row">

      <?php for ($i = 0; $i < 5; $i++): ?>
      <div class="col-lg-12 mb-5">
        <div class="box_video">
          <div class="row">
            <div class="col-12 col-lg-6">
              <div class="box_video_thumb">
                <img src="assets/images/video02.png" class="img-fluid" alt="">
                <div class="carousel-caption d-flex justify-content-center align-items-center">
                  <a href="" class="btn btn-light rounded-pill px-5 py-2">assistir vídeo <img src="assets/images/icones/arrow-blue.svg" alt="" class="ml-3"></a>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center">
              <div class="box_video_text px-5 py-3 py-md-0">
                <a href="" class="text-decoration-none"><h3>Administradora oferece mini mercado para os condomínios</h3></a>
                <p>Para minimizar o impacto da Pandemia nos condomínios, a Cipa oferece o Combo Bem Estar ...</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endfor; ?>

    </div>

    <div class="main_cipa_pagination">
      <div class="row">
        <div class="col-12">
          <nav aria-label="...">
            <ul class="pagination">

              <li class="page-item">
                <a class="page-link" href="#"><img src="assets/images/icones/arrow.svg" alt=""></a>
              </li>

              <li class="page-item"><a class="page-link" href="#">1</a></li>

              <li class="page-item active" aria-current="page">
                <a class="page-link" href="#">2</a>
              </li>

              <li class="page-item"><a class="page-link" href="#">3</a></li>

              <li class="page-item">
                <a class="page-link" href="#"><img src="assets/images/icones/arrow.svg" alt=""></a>
              </li>

            </ul>
          </nav>
        </div>
      </div>
    </div>


  </div>
</section>


<section class="main_see_too mt-5 mb-8">
  <div class="container">

    <div class="line bg-tsuru-red mx-auto mb-3"></div>
    <h2 class="text-center mb-5">Veja <span> Também</span></h2>

    <div class="row">

      <div class="col-lg-6 mb-4 mb-lg-0">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/ebooks.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <h3>E-Books</h3>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="box_see_too">
          <div class="row">
            <div class="col-5">
              <a href="" target="_self" class="text-decoration-none">
                <div class="box_see_too_icon d-flex justify-content-center align-items-center">
                  <img src="assets/images/icones/videos.svg" alt="">
                </div>
              </a>
            </div>
            <div class="col-7 d-flex justify-content-center align-items-center">
              <div class="box_see_too_text px-5">
                <h1>Vídeos</h1>
                <p>Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>

<?php require_once('widgets/optin.php'); ?>
<?php require_once('footer.php'); ?>
