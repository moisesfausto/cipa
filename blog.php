<?php require_once('header.php'); ?>
<?php

$subTitle = 'Blog Condomínio';
$title = 'Blog';
$description = '';

?>
<?php include_once('widgets/page-title.php'); ?>

<div class="container">

  <!-- Search -->
  <div class="row">
    <div class="col-12 col-md-4">
      <div class="main_form_search">
        <form action="">
          <div class="input-group">
            <input type="text" class="form-control btn-outline-light" placeholder="O que você procura">
            <div class="input-group-append">
              <button class="btn btn-outline-light" type="button"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /End -->


  <div class="row mt-5 mb-8">
    <!-- Post List -->
    <div class="col-12 col-md-8">
      <div class="main_post_list">
        <div class="row">

          <?php for ($i = 0; $i < 8; $i++): ?>
          <div class="col-12 col-lg-6 mb-5">
            <img src="assets/images/listagem-de-post.png" class="img-fluid" alt="">
            <h2 class="post_list_title mt-4 mb-3">Título da matéria em até duas ou mais linhas para leitura</h2>
            <p class="post_list_description">Muito satisfeito com a qualidade e organização que o Grupo CIPA nos oferece. Tem sido uma experiência única ter a Administração dessa empresa em nosso condomínio.</p>
          </div>
          <?php endfor; ?>

        </div>

        <div class="main_cipa_pagination">
          <div class="row">
            <div class="col-12">
              <nav aria-label="...">
                <ul class="pagination">

                  <li class="page-item">
                    <a class="page-link" href="#"><img src="assets/images/icones/arrow.svg" alt=""></a>
                  </li>

                  <li class="page-item"><a class="page-link" href="#">1</a></li>

                  <li class="page-item active" aria-current="page">
                    <a class="page-link" href="#">2</a>
                  </li>

                  <li class="page-item"><a class="page-link" href="#">3</a></li>

                  <li class="page-item">
                    <a class="page-link" href="#"><img src="assets/images/icones/arrow.svg" alt=""></a>
                  </li>

                </ul>
              </nav>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- SideBar -->
    <div class="col-12 col-md-4">
      <aside class="main_post_sidebar">
        <div class="post_sidebar_category">
          <span>Categorias</span>
          <ul class="mt-4">
            <li><a href="" class="text-decoration-none">Nome da Categoria 01</a></li>
            <li><a href="" class="text-decoration-none">Nome da Categoria 02</a></li>
            <li><a href="" class="text-decoration-none">Nome da Categoria 03</a></li>
            <li><a href="" class="text-decoration-none">Nome da Categoria 04</a></li>
            <li><a href="" class="text-decoration-none">Nome da Categoria 05</a></li>
          </ul>
        </div>
        <div class="post_sidebar_more_ready">
          <span>Os mais lidos</span>
          <ul class="mt-5">
            <li><span>1</span>O dia Online: Congresso Virtual</li>
            <li><span>1</span>Administradora oferece mini mercado para os condomínios</li>
            <li><span>1</span>O dia Online: Congresso Virtual</li>
            <li><span>1</span>Administradora oferece mini mercado para os condomínios</li>
            <li><span>1</span>O dia Online: Congresso Virtual</li>
          </ul>
        </div>

        <div class="post_sidebar_banner">
          <img src="https://via.placeholder.com/370" class="img-fluid" alt="">
        </div>
      </aside>
    </div>




  </div>

</div>

<?php require_once('widgets/optin.php'); ?>
<?php require_once('footer.php'); ?>
